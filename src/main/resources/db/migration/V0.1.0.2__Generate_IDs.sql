CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

ALTER TABLE organization
    ALTER COLUMN id SET DEFAULT uuid_generate_v4();

ALTER TABLE application
    ALTER COLUMN id SET DEFAULT uuid_generate_v4();