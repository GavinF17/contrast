CREATE TABLE organization
(
    id           UUID PRIMARY KEY,
    created_date DATE        NOT NULL DEFAULT NOW(),
    changed_date DATE        NOT NULL DEFAULT NOW(),

    name         VARCHAR(20) NOT NULL
);

CREATE TABLE platform
(
    id   INTEGER PRIMARY KEY,
    name VARCHAR(20) NOT NULL
);

INSERT INTO platform
VALUES (0, 'AWS'),
       (1, 'AZURE'),
       (2, 'GCP');

CREATE TABLE application
(
    id              UUID PRIMARY KEY,
    created_date    DATE        NOT NULL DEFAULT NOW(),
    changed_date    DATE        NOT NULL DEFAULT NOW(),

    organization_id UUID REFERENCES organization (id) ON DELETE CASCADE,
    platform_id     INTEGER     REFERENCES platform (id) ON DELETE SET NULL,

    name            VARCHAR(20) NOT NULL
);