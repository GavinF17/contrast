package com.gavinfenton.contrast.mapper;

import com.gavinfenton.contrast.dto.ApplicationDTO;
import com.gavinfenton.contrast.entity.Application;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface ApplicationMapper {

    ApplicationMapper INSTANCE = Mappers.getMapper(ApplicationMapper.class);

    Application toApplication(ApplicationDTO applicationDTO);

    ApplicationDTO toApplicationDTO(Application application);

}
