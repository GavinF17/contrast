package com.gavinfenton.contrast.mapper;

import com.gavinfenton.contrast.dto.OrganizationDTO;
import com.gavinfenton.contrast.entity.Organization;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface OrganizationMapper {

    OrganizationMapper INSTANCE = Mappers.getMapper(OrganizationMapper.class);

    Organization toOrganization(OrganizationDTO organizationDTO);

    OrganizationDTO toOrganizationDTO(Organization organization);

}
