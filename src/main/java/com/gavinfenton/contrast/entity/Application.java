package com.gavinfenton.contrast.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import java.util.UUID;

@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@Entity
public class Application extends BaseEntity {

    @Column
    private UUID organizationId;

    @Column(name = "platform_id")
    @Enumerated(EnumType.ORDINAL)
    private Platform platform;

    @Column
    private String name;

}
