package com.gavinfenton.contrast.entity;

public enum Platform {
    AWS, AZURE, GCP
}
