package com.gavinfenton.contrast.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import java.util.Date;
import java.util.UUID;

@Data
@MappedSuperclass
public abstract class BaseEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private UUID id;

    @Column(updatable = false)
    private Date createdDate;

    @Column
    private Date changedDate;

    @PrePersist
    protected void onCreate() {
        createdDate = new Date();
        changedDate = new Date();
    }

    @PreUpdate
    protected void onChange() {
        changedDate = new Date();
    }

}
