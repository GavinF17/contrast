package com.gavinfenton.contrast.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.gavinfenton.contrast.entity.Application;
import com.gavinfenton.contrast.repository.ApplicationRepository;
import com.github.fge.jsonpatch.JsonPatch;
import com.github.fge.jsonpatch.JsonPatchException;
import org.hibernate.ObjectNotFoundException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class ApplicationService {

    private final ObjectMapper objectMapper = new ObjectMapper();

    private final ApplicationRepository applicationRepository;
    private final OrganizationService organizationService;

    public ApplicationService(ApplicationRepository applicationRepository, OrganizationService organizationService) {
        this.applicationRepository = applicationRepository;
        this.organizationService = organizationService;
    }

    public Application createApplication(UUID organizationId, Application application) {
        application.setId(null);
        application.setOrganizationId(organizationId);

        return applicationRepository.saveAndFlush(application);
    }

    public Application getApplication(UUID id) {
        return applicationRepository.findById(id)
                .orElseThrow(() -> new ObjectNotFoundException(id, "Application"));
    }

    public Page<Application> getOrganizationApplications(UUID organizationId, String nameFilter, Pageable pageable) {
        // Ensure org exists
        organizationService.getOrganization(organizationId);

        return applicationRepository.findAllByOrganizationIdAndNameContains(organizationId, nameFilter, pageable);
    }

    private Application updateApplication(Application current, Application updated) {
        // Set updatable fields only
        current.setName(updated.getName());
        current.setPlatform(updated.getPlatform());

        return applicationRepository.saveAndFlush(current);
    }

    public Application updateApplication(UUID applicationId, Application updated) {
        return updateApplication(getApplication(applicationId), updated);
    }

    public Application patchApplication(UUID applicationId, JsonPatch applicationPatch) throws JsonPatchException, JsonProcessingException {
        Application currentApplication = getApplication(applicationId);

        JsonNode patchedJson = applicationPatch.apply(objectMapper.convertValue(currentApplication, JsonNode.class));
        Application patchedApplication = objectMapper.treeToValue(patchedJson, Application.class);

        return updateApplication(currentApplication, patchedApplication);
    }

    public void deleteApplication(UUID applicationId) {
        applicationRepository.deleteById(applicationId);
    }
}
