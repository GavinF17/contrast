package com.gavinfenton.contrast.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.gavinfenton.contrast.entity.Organization;
import com.gavinfenton.contrast.repository.OrganizationRepository;
import com.github.fge.jsonpatch.JsonPatch;
import com.github.fge.jsonpatch.JsonPatchException;
import org.hibernate.ObjectNotFoundException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class OrganizationService {

    private final ObjectMapper objectMapper = new ObjectMapper();

    private final OrganizationRepository organizationRepository;

    public OrganizationService(OrganizationRepository organizationRepository) {
        this.organizationRepository = organizationRepository;
    }

    public Organization createOrganization(Organization organization) {
        organization.setId(null);

        return organizationRepository.saveAndFlush(organization);
    }

    public Organization getOrganization(UUID id) {
        return organizationRepository.findById(id)
                .orElseThrow(() -> new ObjectNotFoundException(id, "Organization"));
    }

    public Page<Organization> getOrganizations(Pageable pageable) {
        return organizationRepository.findAll(pageable);
    }

    private Organization updateOrganization(Organization current, Organization updated) {
        // Set updatable fields only
        current.setName(updated.getName());

        return organizationRepository.saveAndFlush(current);
    }

    public Organization updateOrganization(UUID organizationId, Organization updated) {
        return updateOrganization(getOrganization(organizationId), updated);
    }

    public Organization patchOrganization(UUID organizationId, JsonPatch organizationPatch) throws JsonPatchException, JsonProcessingException {
        Organization currentOrganization = getOrganization(organizationId);

        JsonNode patchedJson = organizationPatch.apply(objectMapper.convertValue(currentOrganization, JsonNode.class));
        Organization patchedOrganization = objectMapper.treeToValue(patchedJson, Organization.class);

        return updateOrganization(currentOrganization, patchedOrganization);
    }

    public void deleteOrganization(UUID organizationId) {
        organizationRepository.deleteById(organizationId);
    }

}
