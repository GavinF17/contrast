package com.gavinfenton.contrast.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@NoArgsConstructor
public class ApiErrorDTO {

    private Date timestamp;
    private String message;
    private Integer status;
    private String error;

    public ApiErrorDTO(String message, Integer status, String error) {
        timestamp = new Date();
        this.message = message;
        this.status = status;
        this.error = error;
    }

}
