package com.gavinfenton.contrast.dto;

import com.gavinfenton.contrast.entity.Platform;
import lombok.Data;

import java.util.UUID;

@Data
public class ApplicationDTO {

    private UUID id;
    
    private UUID organizationId;

    private String name;

    private Platform platform;

}
