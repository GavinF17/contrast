package com.gavinfenton.contrast.constant;

public interface Endpoint {

    String JSON_PATCH = "application/json-patch+json";

    String V1 = "/api/v1";

    String ORG_ID = "orgId";
    String APP_ID = "appId";

    String ORGANIZATIONS = "/organizations";
    String ORGANIZATION = ORGANIZATIONS + "/{" + ORG_ID + "}";

    String APPLICATIONS = "/applications";
    String APPLICATION = APPLICATIONS + "/{" + APP_ID + "}";

}
