package com.gavinfenton.contrast.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.lang.NonNull;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Slf4j
public class LoggingInterceptor extends HandlerInterceptorAdapter {

    @Override
    public boolean preHandle(@NonNull HttpServletRequest request,
                             @NonNull HttpServletResponse response,
                             @NonNull Object handler) {

        log.debug("Started processing [{}]: [{}], handler is [{}]",
                request.getMethod(), request.getRequestURI(), getHandlerName(handler));

        return true;
    }

    @Override
    public void postHandle(HttpServletRequest request,
                           @NonNull HttpServletResponse response,
                           @NonNull Object handler,
                           ModelAndView modelAndView) {

        log.debug("Finished processing [{}]: [{}]; handler was [{}]; response code is [{}]",
                request.getMethod(), request.getRequestURI(), getHandlerName(handler), response.getStatus());
    }

    private String getHandlerName(Object handler) {
        try {
            HandlerMethod handlerMethod = (HandlerMethod) handler;

            return handlerMethod.getMethod().getName();
        } catch (ClassCastException exception) {
            return "unknown method";
        }
    }

}
