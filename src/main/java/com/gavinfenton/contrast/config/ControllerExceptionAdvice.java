package com.gavinfenton.contrast.config;

import com.gavinfenton.contrast.dto.ApiErrorDTO;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.ObjectNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.servlet.http.HttpServletRequest;

@Slf4j
@ControllerAdvice
public class ControllerExceptionAdvice {

    private static ApiErrorDTO buildError(HttpStatus httpStatus, String message) {
        return new ApiErrorDTO(message, httpStatus.value(), httpStatus.getReasonPhrase());
    }

    private static ResponseEntity<ApiErrorDTO> buildErrorResponse(Exception exception, HttpStatus httpStatus, String message, HttpServletRequest request) {
        log.info("Exception while processing [{}]: [{}]; exception is [{}] response code is [{}]",
                request.getMethod(), request.getRequestURI(), exception.getClass(), httpStatus.value());

        return ResponseEntity.status(httpStatus).body(buildError(httpStatus, message));
    }

    @ExceptionHandler(ObjectNotFoundException.class)
    public ResponseEntity<ApiErrorDTO> handleObjectNotFoundException(ObjectNotFoundException exception, HttpServletRequest request) {
        String message = "No " + exception.getEntityName() + " found with ID of '" + exception.getIdentifier() + "'";

        return buildErrorResponse(exception, HttpStatus.NOT_FOUND, message, request);
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<ApiErrorDTO> catchAllHandleException(Exception exception, HttpServletRequest request) {
        String message = "There was an issue fulfilling the request, please check your request and try again";

        log.warn("Exception [{}] was not explained gracefully to user, consider adding it to Controller Advice", exception.getClass(), exception);

        return buildErrorResponse(exception, HttpStatus.INTERNAL_SERVER_ERROR, message, request);
    }

}
