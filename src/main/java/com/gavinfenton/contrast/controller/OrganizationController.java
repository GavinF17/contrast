package com.gavinfenton.contrast.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.gavinfenton.contrast.constant.Endpoint;
import com.gavinfenton.contrast.dto.OrganizationDTO;
import com.gavinfenton.contrast.entity.Organization;
import com.gavinfenton.contrast.mapper.OrganizationMapper;
import com.gavinfenton.contrast.service.OrganizationService;
import com.github.fge.jsonpatch.JsonPatch;
import com.github.fge.jsonpatch.JsonPatchException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.net.URI;
import java.util.UUID;

@RestController
public class OrganizationController {

    private final OrganizationMapper organizationMapper = OrganizationMapper.INSTANCE;

    private final OrganizationService organizationService;

    public OrganizationController(OrganizationService organizationService) {
        this.organizationService = organizationService;
    }

    @PostMapping(Endpoint.V1 + Endpoint.ORGANIZATIONS)
    public ResponseEntity<OrganizationDTO> createOrganization(@RequestBody @Valid OrganizationDTO organizationDTO) {
        Organization created = organizationService.createOrganization(organizationMapper.toOrganization(organizationDTO));
        URI location = URI.create(Endpoint.V1 + Endpoint.ORGANIZATIONS + "/" + created.getId());

        return ResponseEntity.created(location).body(organizationMapper.toOrganizationDTO(created));
    }

    @GetMapping(Endpoint.V1 + Endpoint.ORGANIZATIONS)
    public ResponseEntity<Page<OrganizationDTO>> getOrganizations(Pageable pageable) {
        Page<Organization> organizations = organizationService.getOrganizations(pageable);

        return ResponseEntity.ok(organizations.map(organizationMapper::toOrganizationDTO));
    }

    @GetMapping(Endpoint.V1 + Endpoint.ORGANIZATION)
    public ResponseEntity<OrganizationDTO> getOrganization(@PathVariable(Endpoint.ORG_ID) UUID organisationId) {
        Organization organization = organizationService.getOrganization(organisationId);

        return ResponseEntity.ok(organizationMapper.toOrganizationDTO(organization));
    }

    @PutMapping(Endpoint.V1 + Endpoint.ORGANIZATION)
    public ResponseEntity<OrganizationDTO> updateOrganization(@PathVariable(Endpoint.ORG_ID) UUID organisationId,
                                                              @RequestBody @Valid OrganizationDTO organizationDTO) {
        Organization updated = organizationService.updateOrganization(organisationId, organizationMapper.toOrganization(organizationDTO));

        return ResponseEntity.ok(organizationMapper.toOrganizationDTO(updated));
    }

    @PatchMapping(value = Endpoint.V1 + Endpoint.ORGANIZATION, consumes = Endpoint.JSON_PATCH)
    public ResponseEntity<OrganizationDTO> patchOrganization(@PathVariable(Endpoint.ORG_ID) UUID organisationId,
                                                             @RequestBody JsonPatch organizationPatch) throws JsonPatchException, JsonProcessingException {
        Organization updated = organizationService.patchOrganization(organisationId, organizationPatch);

        return ResponseEntity.ok(organizationMapper.toOrganizationDTO(updated));
    }

    @DeleteMapping(Endpoint.V1 + Endpoint.ORGANIZATION)
    public ResponseEntity<OrganizationDTO> deleteOrganization(@PathVariable(Endpoint.ORG_ID) UUID organisationId) {
        organizationService.deleteOrganization(organisationId);

        return ResponseEntity.noContent().build();
    }

}
