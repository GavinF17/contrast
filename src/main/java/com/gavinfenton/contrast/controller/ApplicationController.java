package com.gavinfenton.contrast.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.gavinfenton.contrast.constant.Endpoint;
import com.gavinfenton.contrast.dto.ApplicationDTO;
import com.gavinfenton.contrast.entity.Application;
import com.gavinfenton.contrast.mapper.ApplicationMapper;
import com.gavinfenton.contrast.service.ApplicationService;
import com.github.fge.jsonpatch.JsonPatch;
import com.github.fge.jsonpatch.JsonPatchException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.net.URI;
import java.util.UUID;

@RestController
public class ApplicationController {

    private final ApplicationMapper applicationMapper = ApplicationMapper.INSTANCE;

    private final ApplicationService applicationService;

    public ApplicationController(ApplicationService applicationService) {
        this.applicationService = applicationService;
    }

    @PostMapping(Endpoint.V1 + Endpoint.ORGANIZATION + Endpoint.APPLICATIONS)
    public ResponseEntity<ApplicationDTO> createApplication(@PathVariable(Endpoint.ORG_ID) UUID organizationId,
                                                            @RequestBody @Valid ApplicationDTO applicationDTO) {
        Application created = applicationService.createApplication(organizationId, applicationMapper.toApplication(applicationDTO));
        URI location = URI.create(Endpoint.V1 + Endpoint.APPLICATIONS + "/" + created.getId());

        return ResponseEntity.created(location).body(applicationMapper.toApplicationDTO(created));
    }

    @GetMapping(Endpoint.V1 + Endpoint.ORGANIZATION + Endpoint.APPLICATIONS)
    public ResponseEntity<Page<ApplicationDTO>> getOrganizationApplications(@PathVariable(Endpoint.ORG_ID) UUID organizationId,
                                                                            Pageable pageable,
                                                                            @RequestParam(defaultValue = "", required = false) String query) {
        Page<Application> applications = applicationService.getOrganizationApplications(organizationId, query, pageable);

        return ResponseEntity.ok(applications.map(applicationMapper::toApplicationDTO));
    }

    @GetMapping(Endpoint.V1 + Endpoint.APPLICATION)
    public ResponseEntity<ApplicationDTO> getApplication(@PathVariable(Endpoint.APP_ID) UUID applicationId) {
        Application application = applicationService.getApplication(applicationId);

        return ResponseEntity.ok(applicationMapper.toApplicationDTO(application));
    }

    @PutMapping(Endpoint.V1 + Endpoint.APPLICATION)
    public ResponseEntity<ApplicationDTO> updateApplication(@PathVariable(Endpoint.APP_ID) UUID applicationId,
                                                            @RequestBody @Valid ApplicationDTO applicationDTO) {
        Application updated = applicationService.updateApplication(applicationId, applicationMapper.toApplication(applicationDTO));

        return ResponseEntity.ok(applicationMapper.toApplicationDTO(updated));
    }

    @PatchMapping(value = Endpoint.V1 + Endpoint.APPLICATION, consumes = Endpoint.JSON_PATCH)
    public ResponseEntity<ApplicationDTO> patchApplication(@PathVariable(Endpoint.APP_ID) UUID applicationId,
                                                           @RequestBody JsonPatch applicationPatch) throws JsonPatchException, JsonProcessingException {
        Application updated = applicationService.patchApplication(applicationId, applicationPatch);

        return ResponseEntity.ok(applicationMapper.toApplicationDTO(updated));
    }

    @DeleteMapping(Endpoint.V1 + Endpoint.APPLICATION)
    public ResponseEntity<ApplicationDTO> deleteApplication(@PathVariable(Endpoint.APP_ID) UUID applicationId) {
        applicationService.deleteApplication(applicationId);

        return ResponseEntity.noContent().build();
    }

}
