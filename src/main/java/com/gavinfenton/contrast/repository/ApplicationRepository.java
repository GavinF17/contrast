package com.gavinfenton.contrast.repository;

import com.gavinfenton.contrast.entity.Application;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface ApplicationRepository extends JpaRepository<Application, UUID> {

    Page<Application> findAllByOrganizationIdAndNameContains(UUID organizationId, String name, Pageable pageable);

}
