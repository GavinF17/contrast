package utils;

import com.fasterxml.jackson.databind.module.SimpleModule;
import org.springframework.data.domain.Page;

/**
 * Disclaimer: I have previously used this in work after finding it on StackOverflow
 *
 * Credit: Mikhail Kopylov - https://stackoverflow.com/a/54730375/4249841
 */
public class PageModule extends SimpleModule {

    private static final long serialVersionUID = 1L;

    public PageModule() {
        addDeserializer(Page.class, new PageDeserializer());
    }

}