package utils;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonTokenId;
import com.fasterxml.jackson.databind.BeanProperty;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.deser.ContextualDeserializer;
import com.fasterxml.jackson.databind.type.CollectionType;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Disclaimer: I have previously used this in work after finding it on StackOverflow
 *
 * Credit: Mikhail Kopylov - https://stackoverflow.com/a/54730375/4249841
 */
public class PageDeserializer extends JsonDeserializer<Page<?>> implements ContextualDeserializer {

    private static final String CONTENT = "content";
    private static final String NUMBER = "number";
    private static final String SIZE = "size";
    private static final String TOTAL_ELEMENTS = "totalElements";
    private JavaType valueType;

    @Override
    public Page<?> deserialize(JsonParser p, DeserializationContext context) throws IOException {
        final CollectionType valuesListType = context.getTypeFactory().constructCollectionType(List.class, valueType);

        List<?> list = new ArrayList<>();
        int pageNumber = 0;
        int pageSize = 0;
        long total = 0;
        if (p.isExpectedStartObjectToken()) {
            p.nextToken();
            if (p.hasTokenId(JsonTokenId.ID_FIELD_NAME)) {
                String propName = p.getCurrentName();
                do {
                    p.nextToken();
                    switch (propName) {
                        case CONTENT:
                            list = context.readValue(p, valuesListType);
                            break;
                        case NUMBER:
                            pageNumber = context.readValue(p, Integer.class);
                            break;
                        case SIZE:
                            pageSize = context.readValue(p, Integer.class);
                            break;
                        case TOTAL_ELEMENTS:
                            total = context.readValue(p, Long.class);
                            break;
                        default:
                            p.skipChildren();
                            break;
                    }
                } while (((propName = p.nextFieldName())) != null);
            } else {
                context.handleUnexpectedToken(handledType(), p);
            }
        } else {
            context.handleUnexpectedToken(handledType(), p);
        }

        return new PageImpl<>(list, PageRequest.of(pageNumber, pageSize), total);
    }

    @Override
    public JsonDeserializer<?> createContextual(DeserializationContext context, BeanProperty property) {
        final JavaType wrapperType = context.getContextualType();
        final PageDeserializer deserializer = new PageDeserializer();
        deserializer.valueType = wrapperType.containedType(0);
        return deserializer;
    }

}