package utils;

import com.github.fge.jsonpatch.JsonPatch;
import org.mockito.ArgumentMatcher;

public class JsonPatchMatcher implements ArgumentMatcher<JsonPatch> {

    private final JsonPatch left;

    public JsonPatchMatcher(JsonPatch left) {
        this.left = left;
    }

    @Override
    public boolean matches(JsonPatch right) {
        return left.toString().equals(right.toString());
    }

}