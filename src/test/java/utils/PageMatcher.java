package utils;

import org.mockito.ArgumentMatcher;
import org.springframework.data.domain.Page;

public class PageMatcher implements ArgumentMatcher<Page<?>> {

    private final Page<?> left;

    public PageMatcher(Page<?> left) {
        this.left = left;
    }

    @Override
    public boolean matches(Page<?> right) {
        return left.getTotalElements() == right.getTotalElements()
                && left.getTotalPages() == right.getTotalPages()
                && left.getNumber() == right.getNumber()
                && left.getNumberOfElements() == right.getNumberOfElements()
                && left.getContent().equals(right.getContent());
    }

}