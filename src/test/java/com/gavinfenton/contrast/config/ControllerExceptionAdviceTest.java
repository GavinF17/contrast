package com.gavinfenton.contrast.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.gavinfenton.contrast.controller.OrganizationController;
import com.gavinfenton.contrast.dto.ApiErrorDTO;
import com.gavinfenton.contrast.service.OrganizationService;
import org.hibernate.ObjectNotFoundException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.awt.color.CMMException;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(OrganizationController.class)
public class ControllerExceptionAdviceTest {

    private final ObjectMapper objectMapper = new ObjectMapper();

    @MockBean
    private OrganizationService organizationService;

    @Autowired
    private MockMvc mockMvc;

    @BeforeEach
    public void setup() {
        initMocks(this);
    }

    @Test
    public void testObjectNotFoundExceptionReturns404() throws Exception {
        // Given
        UUID organizationId = UUID.randomUUID();
        ObjectNotFoundException exception = new ObjectNotFoundException(organizationId, "Organization");
        given(organizationService.getOrganization(any())).willThrow(exception);

        MockHttpServletRequestBuilder request = MockMvcRequestBuilders
                .get("/api/v1/organizations/" + organizationId);

        // When
        ResultActions response = mockMvc.perform(request);
        ApiErrorDTO errorDTO = objectMapper.readValue(response.andReturn().getResponse().getContentAsString(), ApiErrorDTO.class);

        // Then
        verify(organizationService).getOrganization(organizationId);
        response.andExpect(status().isNotFound());
        assertNotNull(errorDTO.getTimestamp());
        assertTrue(errorDTO.getMessage().contains(organizationId.toString()));
        assertTrue(errorDTO.getMessage().contains("Organization"));
        assertEquals(HttpStatus.NOT_FOUND.value(), errorDTO.getStatus());
        assertEquals(HttpStatus.NOT_FOUND.getReasonPhrase(), errorDTO.getError());
    }

    @Test
    public void testUnhandledExceptionReturns500() throws Exception {
        // Given
        UUID organizationId = UUID.randomUUID();
        CMMException exception = new CMMException("");
        given(organizationService.getOrganization(any())).willThrow(exception);

        MockHttpServletRequestBuilder request = MockMvcRequestBuilders
                .get("/api/v1/organizations/" + organizationId);

        // When
        ResultActions response = mockMvc.perform(request);
        ApiErrorDTO errorDTO = objectMapper.readValue(response.andReturn().getResponse().getContentAsString(), ApiErrorDTO.class);

        // Then
        verify(organizationService).getOrganization(organizationId);
        response.andExpect(status().isInternalServerError());
        assertNotNull(errorDTO.getTimestamp());
        assertNotNull(errorDTO.getMessage());
        assertEquals(HttpStatus.INTERNAL_SERVER_ERROR.value(), errorDTO.getStatus());
        assertEquals(HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase(), errorDTO.getError());
    }

}
