package com.gavinfenton.contrast.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.node.TextNode;
import com.gavinfenton.contrast.entity.Organization;
import com.gavinfenton.contrast.entity.Platform;
import com.gavinfenton.contrast.repository.OrganizationRepository;
import com.github.fge.jackson.jsonpointer.JsonPointer;
import com.github.fge.jackson.jsonpointer.JsonPointerException;
import com.github.fge.jsonpatch.JsonPatch;
import com.github.fge.jsonpatch.JsonPatchException;
import com.github.fge.jsonpatch.ReplaceOperation;
import org.hibernate.ObjectNotFoundException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import utils.PageMatcher;

import java.util.Arrays;
import java.util.Collections;
import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;

public class OrganizationServiceTest {

    @InjectMocks
    private OrganizationService organizationService;

    @Mock
    private OrganizationRepository organizationRepository;

    @BeforeEach
    public void setup() {
        initMocks(this);
    }

    @Test
    public void testGetOrganizationCallsAndReturnsFromRepository() {
        // Given
        UUID organizationId = UUID.randomUUID();

        Organization expectedOrganization = new Organization();
        expectedOrganization.setId(UUID.randomUUID());
        expectedOrganization.setName("Contrast");

        given(organizationRepository.findById(organizationId)).willReturn(Optional.of(expectedOrganization));

        // When
        Organization actualOrganization = organizationService.getOrganization(organizationId);

        // Then
        verify(organizationRepository).findById(organizationId);
        assertEquals(expectedOrganization, actualOrganization);
    }

    @Test
    public void testGetOrganizationCallsAndThrowsWhenEmpty() {
        // Given
        UUID organizationId = UUID.randomUUID();

        given(organizationRepository.findById(organizationId)).willReturn(Optional.empty());

        // When / Then
        assertThrows(ObjectNotFoundException.class, () -> organizationService.getOrganization(organizationId));
        verify(organizationRepository).findById(organizationId);
    }

    @Test
    public void testGetOrganizationsCallsAndReturnsFromRepository() {
        // Given
        Pageable pageRequest = PageRequest.of(0, 20);

        Organization returnedOrganization = new Organization();
        returnedOrganization.setId(UUID.randomUUID());
        Page<Organization> expectedOrganizationPage = new PageImpl<>(Collections.singletonList(returnedOrganization), pageRequest, 1);
        given(organizationRepository.findAll(pageRequest)).willReturn(expectedOrganizationPage);

        // When
        Page<Organization> actualOrganizationPage = organizationService.getOrganizations(pageRequest);

        // Then
        verify(organizationRepository).findAll(pageRequest);
        assertTrue(new PageMatcher(expectedOrganizationPage).matches(actualOrganizationPage));
    }

    @Test
    public void testUpdateGetsCurrentFromRepoThenProcessesThenSavesAndReturnsFromRepository() {
        // Given
        UUID updatingId = UUID.randomUUID();
        UUID organizationId = UUID.randomUUID();
        String updatedName = "Assess";

        Organization organizationUpdating = new Organization();
        organizationUpdating.setId(UUID.randomUUID());
        organizationUpdating.setName(updatedName);

        Organization organizationCurrent = new Organization();
        organizationCurrent.setId(updatingId);
        organizationCurrent.setName("Protect");
        given(organizationRepository.findById(updatingId)).willReturn(Optional.of(organizationCurrent));

        Organization organizationProcessed = new Organization();
        organizationProcessed.setId(updatingId);
        organizationProcessed.setName(updatedName);

        Organization organizationExpected = new Organization();
        organizationExpected.setId(UUID.randomUUID());
        given(organizationRepository.saveAndFlush(organizationProcessed)).willReturn(organizationExpected);

        // When
        Organization organizationActual = organizationService.updateOrganization(updatingId, organizationUpdating);

        // Then
        verify(organizationRepository).findById(updatingId);
        verify(organizationRepository).saveAndFlush(organizationProcessed);
        assertEquals(organizationExpected, organizationActual);
    }

    @Test
    public void testPatchUpdatesAllowedFieldsThenSavesAndReturnsFromRepository() throws JsonPointerException, JsonPatchException, JsonProcessingException {
        // Given
        UUID updatingId = UUID.randomUUID();
        UUID organizationId = UUID.randomUUID();
        String updatedName = "Assess";
        Platform updatedPlatform = Platform.AWS;

        JsonPatch organizationPatch = new JsonPatch(Arrays.asList(
                new ReplaceOperation(new JsonPointer("/id"), TextNode.valueOf(UUID.randomUUID().toString())),
                new ReplaceOperation(new JsonPointer("/name"), TextNode.valueOf(updatedName))
        ));

        Organization organizationCurrent = new Organization();
        organizationCurrent.setId(updatingId);
        organizationCurrent.setName("Protect");
        given(organizationRepository.findById(updatingId)).willReturn(Optional.of(organizationCurrent));

        Organization organizationProcessed = new Organization();
        organizationProcessed.setId(updatingId);
        organizationProcessed.setName(updatedName);

        Organization organizationExpected = new Organization();
        organizationExpected.setId(UUID.randomUUID());
        given(organizationRepository.saveAndFlush(organizationProcessed)).willReturn(organizationExpected);

        // When
        Organization organizationActual = organizationService.patchOrganization(updatingId, organizationPatch);

        // Then
        verify(organizationRepository).findById(updatingId);
        verify(organizationRepository).saveAndFlush(organizationProcessed);
        assertEquals(organizationExpected, organizationActual);
    }

    @Test
    public void testDeleteOrganizationCallsRepositoryWithId() {
        // Given
        UUID deletingId = UUID.randomUUID();

        // When
        organizationService.deleteOrganization(deletingId);

        // Then
        verify(organizationRepository).deleteById(deletingId);
    }

}
