package com.gavinfenton.contrast.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.node.TextNode;
import com.gavinfenton.contrast.entity.Application;
import com.gavinfenton.contrast.entity.Platform;
import com.gavinfenton.contrast.repository.ApplicationRepository;
import com.github.fge.jackson.jsonpointer.JsonPointer;
import com.github.fge.jackson.jsonpointer.JsonPointerException;
import com.github.fge.jsonpatch.JsonPatch;
import com.github.fge.jsonpatch.JsonPatchException;
import com.github.fge.jsonpatch.ReplaceOperation;
import org.hibernate.ObjectNotFoundException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import utils.PageMatcher;

import java.util.Arrays;
import java.util.Collections;
import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;

public class ApplicationServiceTest {

    @InjectMocks
    private ApplicationService applicationService;

    @Mock
    private ApplicationRepository applicationRepository;

    @Mock
    private OrganizationService organizationService;

    @BeforeEach
    public void setup() {
        initMocks(this);
    }

    @Test
    public void testCreateApplicationProcessesApplicationThenSavesAndReturnsFromRepository() {
        // Given
        UUID organizationId = UUID.randomUUID();

        Application applicationCreating = new Application();
        applicationCreating.setId(UUID.randomUUID());
        applicationCreating.setOrganizationId(UUID.randomUUID());
        applicationCreating.setName("Assess");

        Application applicationProcessed = new Application();
        applicationProcessed.setOrganizationId(organizationId);
        applicationProcessed.setName("Assess");

        Application applicationExpected = new Application();
        applicationExpected.setId(UUID.randomUUID());
        given(applicationRepository.saveAndFlush(applicationProcessed)).willReturn(applicationExpected);

        // When
        Application applicationActual = applicationService.createApplication(organizationId, applicationCreating);

        // Then
        verify(applicationRepository).saveAndFlush(applicationProcessed);
        assertEquals(applicationExpected, applicationActual);
    }

    @Test
    public void testGetApplicationCallsAndReturnsFromRepository() {
        // Given
        UUID applicationId = UUID.randomUUID();

        Application expectedApplication = new Application();
        expectedApplication.setId(UUID.randomUUID());
        expectedApplication.setName("Protect");

        given(applicationRepository.findById(applicationId)).willReturn(Optional.of(expectedApplication));

        // When
        Application actualApplication = applicationService.getApplication(applicationId);

        // Then
        verify(applicationRepository).findById(applicationId);
        assertEquals(expectedApplication, actualApplication);
    }

    @Test
    public void testGetApplicationCallsAndThrowsWhenEmpty() {
        // Given
        UUID applicationId = UUID.randomUUID();

        given(applicationRepository.findById(applicationId)).willReturn(Optional.empty());

        // When / Then
        assertThrows(ObjectNotFoundException.class, () -> applicationService.getApplication(applicationId));
        verify(applicationRepository).findById(applicationId);
    }

    @Test
    public void testGetApplicationsChecksOrgExistsThenCallsAndReturnsFromRepository() {
        // Given
        UUID organizationId = UUID.randomUUID();
        String nameFilter = "Contr";
        Pageable pageRequest = PageRequest.of(0, 20);

        Application returnedApplication = new Application();
        returnedApplication.setId(UUID.randomUUID());
        Page<Application> expectedApplicationPage = new PageImpl<>(Collections.singletonList(returnedApplication), pageRequest, 1);
        given(applicationRepository.findAllByOrganizationIdAndNameContains(organizationId, nameFilter, pageRequest)).willReturn(expectedApplicationPage);

        // When
        Page<Application> actualApplicationPage = applicationService.getOrganizationApplications(organizationId, nameFilter, pageRequest);

        // Then
        verify(organizationService).getOrganization(organizationId);
        verify(applicationRepository).findAllByOrganizationIdAndNameContains(organizationId, nameFilter, pageRequest);
        assertTrue(new PageMatcher(expectedApplicationPage).matches(actualApplicationPage));
    }

    @Test
    public void testUpdateGetsCurrentFromRepoThenProcessesThenSavesAndReturnsFromRepository() {
        // Given
        UUID updatingId = UUID.randomUUID();
        UUID organizationId = UUID.randomUUID();
        String updatedName = "Assess";
        Platform updatedPlatform = Platform.AWS;

        Application applicationUpdating = new Application();
        applicationUpdating.setId(UUID.randomUUID());
        applicationUpdating.setOrganizationId(UUID.randomUUID());
        applicationUpdating.setName(updatedName);
        applicationUpdating.setPlatform(updatedPlatform);

        Application applicationCurrent = new Application();
        applicationCurrent.setId(updatingId);
        applicationCurrent.setOrganizationId(organizationId);
        applicationCurrent.setName("Protect");
        applicationCurrent.setPlatform(Platform.GCP);
        given(applicationRepository.findById(updatingId)).willReturn(Optional.of(applicationCurrent));

        Application applicationProcessed = new Application();
        applicationProcessed.setId(updatingId);
        applicationProcessed.setOrganizationId(organizationId);
        applicationProcessed.setName(updatedName);
        applicationProcessed.setPlatform(updatedPlatform);

        Application applicationExpected = new Application();
        applicationExpected.setId(UUID.randomUUID());
        given(applicationRepository.saveAndFlush(applicationProcessed)).willReturn(applicationExpected);

        // When
        Application applicationActual = applicationService.updateApplication(updatingId, applicationUpdating);

        // Then
        verify(applicationRepository).findById(updatingId);
        verify(applicationRepository).saveAndFlush(applicationProcessed);
        assertEquals(applicationExpected, applicationActual);
    }

    @Test
    public void testPatchUpdatesAllowedFieldsThenSavesAndReturnsFromRepository() throws JsonPointerException, JsonPatchException, JsonProcessingException {
        // Given
        UUID updatingId = UUID.randomUUID();
        UUID organizationId = UUID.randomUUID();
        String updatedName = "Assess";
        Platform updatedPlatform = Platform.AWS;

        JsonPatch applicationPatch = new JsonPatch(Arrays.asList(
                new ReplaceOperation(new JsonPointer("/id"), TextNode.valueOf(UUID.randomUUID().toString())),
                new ReplaceOperation(new JsonPointer("/organizationId"), TextNode.valueOf(UUID.randomUUID().toString())),
                new ReplaceOperation(new JsonPointer("/name"), TextNode.valueOf(updatedName)),
                new ReplaceOperation(new JsonPointer("/platform"), TextNode.valueOf(updatedPlatform.toString()))
        ));

        Application applicationCurrent = new Application();
        applicationCurrent.setId(updatingId);
        applicationCurrent.setOrganizationId(organizationId);
        applicationCurrent.setName("Protect");
        applicationCurrent.setPlatform(Platform.GCP);
        given(applicationRepository.findById(updatingId)).willReturn(Optional.of(applicationCurrent));

        Application applicationProcessed = new Application();
        applicationProcessed.setId(updatingId);
        applicationProcessed.setOrganizationId(organizationId);
        applicationProcessed.setName(updatedName);
        applicationProcessed.setPlatform(updatedPlatform);

        Application applicationExpected = new Application();
        applicationExpected.setId(UUID.randomUUID());
        given(applicationRepository.saveAndFlush(applicationProcessed)).willReturn(applicationExpected);

        // When
        Application applicationActual = applicationService.patchApplication(updatingId, applicationPatch);

        // Then
        verify(applicationRepository).findById(updatingId);
        verify(applicationRepository).saveAndFlush(applicationProcessed);
        assertEquals(applicationExpected, applicationActual);
    }

    @Test
    public void testDeleteApplicationCallsRepositoryWithId() {
        // Given
        UUID deletingId = UUID.randomUUID();

        // When
        applicationService.deleteApplication(deletingId);

        // Then
        verify(applicationRepository).deleteById(deletingId);
    }


}
