package com.gavinfenton.contrast.mapper;

import com.gavinfenton.contrast.dto.OrganizationDTO;
import com.gavinfenton.contrast.entity.Organization;
import org.junit.jupiter.api.Test;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

public class OrganizationMapperTest {

    private final OrganizationMapper organizationMapper = OrganizationMapper.INSTANCE;

    @Test
    public void testAllOrganizationDTOFieldsAreMappedFromOrganization() {
        // Given
        UUID organizationId = UUID.randomUUID();
        String name = "Contrast";

        Organization organization = new Organization();
        organization.setId(organizationId);
        organization.setName(name);

        OrganizationDTO expectedDTO = new OrganizationDTO();
        expectedDTO.setId(organizationId);
        expectedDTO.setName(name);

        // When
        OrganizationDTO actualDTO = organizationMapper.toOrganizationDTO(organization);

        // Then
        assertEquals(expectedDTO, actualDTO);
    }

    @Test
    public void testAllRelevantOrganizationFieldsAreMappedFromOrganizationDTO() {
        // Given
        UUID organizationId = UUID.randomUUID();
        String name = "Contrast";

        OrganizationDTO organizationDTO = new OrganizationDTO();
        organizationDTO.setId(organizationId);
        organizationDTO.setName(name);

        // When
        Organization actualOrganization = organizationMapper.toOrganization(organizationDTO);

        // Then
        assertEquals(organizationId, actualOrganization.getId());
        assertNull(actualOrganization.getCreatedDate());
        assertNull(actualOrganization.getChangedDate());
        assertEquals(name, actualOrganization.getName());
    }

}
