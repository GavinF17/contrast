package com.gavinfenton.contrast.mapper;

import com.gavinfenton.contrast.dto.ApplicationDTO;
import com.gavinfenton.contrast.entity.Application;
import com.gavinfenton.contrast.entity.Platform;
import org.junit.jupiter.api.Test;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

public class ApplicationMapperTest {

    private final ApplicationMapper applicationMapper = ApplicationMapper.INSTANCE;

    @Test
    public void testAllApplicationDTOFieldsAreMappedFromApplication() {
        // Given
        UUID applicationId = UUID.randomUUID();
        String name = "Contrast Protect";
        Platform platform = Platform.AWS;

        Application application = new Application();
        application.setId(applicationId);
        application.setName(name);
        application.setPlatform(platform);

        ApplicationDTO expectedDTO = new ApplicationDTO();
        expectedDTO.setId(applicationId);
        expectedDTO.setName(name);
        expectedDTO.setPlatform(platform);

        // When
        ApplicationDTO actualDTO = applicationMapper.toApplicationDTO(application);

        // Then
        assertEquals(expectedDTO, actualDTO);
    }

    @Test
    public void testAllRelevantApplicationFieldsAreMappedFromApplicationDTO() {
        // Given
        UUID applicationId = UUID.randomUUID();
        UUID organizationId = UUID.randomUUID();
        String name = "Contrast Protect";
        Platform platform = Platform.AWS;

        ApplicationDTO applicationDTO = new ApplicationDTO();
        applicationDTO.setId(applicationId);
        applicationDTO.setOrganizationId(organizationId);
        applicationDTO.setName(name);
        applicationDTO.setPlatform(platform);

        // When
        Application actualApplication = applicationMapper.toApplication(applicationDTO);

        // Then
        assertEquals(applicationId, actualApplication.getId());
        assertEquals(organizationId, actualApplication.getOrganizationId());
        assertNull(actualApplication.getCreatedDate());
        assertNull(actualApplication.getChangedDate());
        assertEquals(name, actualApplication.getName());
        assertEquals(platform, actualApplication.getPlatform());
    }

}
