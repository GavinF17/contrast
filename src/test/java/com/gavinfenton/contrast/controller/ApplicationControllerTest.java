package com.gavinfenton.contrast.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.TextNode;
import com.gavinfenton.contrast.constant.Endpoint;
import com.gavinfenton.contrast.dto.ApplicationDTO;
import com.gavinfenton.contrast.entity.Application;
import com.gavinfenton.contrast.entity.Platform;
import com.gavinfenton.contrast.mapper.ApplicationMapper;
import com.gavinfenton.contrast.service.ApplicationService;
import com.github.fge.jackson.jsonpointer.JsonPointer;
import com.github.fge.jsonpatch.JsonPatch;
import com.github.fge.jsonpatch.JsonPatchOperation;
import com.github.fge.jsonpatch.ReplaceOperation;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import utils.JsonPatchMatcher;
import utils.PageMatcher;
import utils.PageModule;

import java.util.Collections;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.argThat;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(ApplicationController.class)
public class ApplicationControllerTest {

    private final ApplicationMapper applicationMapper = ApplicationMapper.INSTANCE;

    private final ObjectMapper objectMapper = new ObjectMapper();

    @MockBean
    private ApplicationService applicationService;

    @Autowired
    private MockMvc mockMvc;

    @BeforeEach
    public void setup() {
        initMocks(this);

        objectMapper.registerModule(new PageModule());
    }

    @Test
    public void testCreateApplicationCallsServiceWithAppAndReturnsFromServiceWithLocationHeader() throws Exception {
        // Given
        UUID organizationId = UUID.randomUUID();
        ApplicationDTO dtoCreating = new ApplicationDTO();
        dtoCreating.setName("Contrast OSS");
        dtoCreating.setPlatform(Platform.AWS);
        Application applicationCreating = applicationMapper.toApplication(dtoCreating);

        Application applicationReturned = new Application();
        UUID returnedId = UUID.randomUUID();
        applicationReturned.setId(returnedId);
        given(applicationService.createApplication(organizationId, applicationCreating)).willReturn(applicationReturned);

        ApplicationDTO dtoExpected = applicationMapper.toApplicationDTO(applicationReturned);

        MockHttpServletRequestBuilder request = MockMvcRequestBuilders
                .post("/api/v1/organizations/" + organizationId + "/applications")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(dtoCreating));

        // When
        ResultActions response = mockMvc.perform(request);
        ApplicationDTO dtoActual = objectMapper.readValue(response.andReturn().getResponse().getContentAsString(), ApplicationDTO.class);

        // Then
        verify(applicationService).createApplication(organizationId, applicationCreating);
        response.andExpect(status().isCreated());
        response.andExpect(header().string("Location", "/api/v1/applications/" + returnedId));
        assertEquals(dtoExpected, dtoActual);
    }

    @Test
    public void testGetApplicationsCallsServiceAndReturnsDTOPage() throws Exception {
        // Given
        UUID organizationId = UUID.randomUUID();
        String nameFilter = "Contr";
        int size = 25;
        Pageable pageRequest = PageRequest.of(0, size);

        Application returnedApplication = new Application();
        returnedApplication.setId(UUID.randomUUID());
        Page<Application> returnedApplicationPage = new PageImpl<>(Collections.singletonList(returnedApplication), pageRequest, 1);
        given(applicationService.getOrganizationApplications(organizationId, nameFilter, pageRequest)).willReturn(returnedApplicationPage);

        Page<ApplicationDTO> expectedDTOPage = returnedApplicationPage.map(applicationMapper::toApplicationDTO);

        MockHttpServletRequestBuilder request = MockMvcRequestBuilders
                .get("/api/v1/organizations/" + organizationId + "/applications?query=" + nameFilter + "&size=" + size);

        // When
        ResultActions response = mockMvc.perform(request);
        Page<ApplicationDTO> actualDTOPage = objectMapper.readValue(response.andReturn().getResponse().getContentAsString(), new TypeReference<>() {

        });

        // Then
        verify(applicationService).getOrganizationApplications(organizationId, nameFilter, pageRequest);
        response.andExpect(status().isOk());
        assertTrue(new PageMatcher(expectedDTOPage).matches(actualDTOPage));
    }

    @Test
    public void testGetApplicationCallsServiceWithIdAndReturnsDTO() throws Exception {
        // Given
        UUID applicationId = UUID.randomUUID();

        Application returnedApplication = new Application();
        returnedApplication.setId(UUID.randomUUID());
        given(applicationService.getApplication(applicationId)).willReturn(returnedApplication);

        ApplicationDTO expectedDTO = applicationMapper.toApplicationDTO(returnedApplication);

        MockHttpServletRequestBuilder request = MockMvcRequestBuilders
                .get("/api/v1/applications/" + applicationId);

        // When
        ResultActions response = mockMvc.perform(request);
        ApplicationDTO actualDTO = objectMapper.readValue(response.andReturn().getResponse().getContentAsString(), ApplicationDTO.class);

        // Then
        verify(applicationService).getApplication(applicationId);
        response.andExpect(status().isOk());
        assertEquals(expectedDTO, actualDTO);
    }

    @Test
    public void testUpdateApplicationCallsServiceWithAppAndReturnsFromService() throws Exception {
        // Given
        UUID applicationId = UUID.randomUUID();
        ApplicationDTO dtoUpdating = new ApplicationDTO();
        dtoUpdating.setName("Contrast OSS");
        dtoUpdating.setPlatform(Platform.AWS);
        Application applicationCreating = applicationMapper.toApplication(dtoUpdating);

        Application applicationReturned = new Application();
        UUID returnedId = UUID.randomUUID();
        applicationReturned.setId(returnedId);
        given(applicationService.updateApplication(applicationId, applicationCreating)).willReturn(applicationReturned);

        ApplicationDTO dtoExpected = applicationMapper.toApplicationDTO(applicationReturned);

        MockHttpServletRequestBuilder request = MockMvcRequestBuilders
                .put("/api/v1/applications/" + applicationId)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(dtoUpdating));

        // When
        ResultActions response = mockMvc.perform(request);
        ApplicationDTO dtoActual = objectMapper.readValue(response.andReturn().getResponse().getContentAsString(), ApplicationDTO.class);

        // Then
        verify(applicationService).updateApplication(applicationId, applicationCreating);
        response.andExpect(status().isOk());
        assertEquals(dtoExpected, dtoActual);
    }

    @Test
    public void testPatchApplicationCallsServiceWithAppAndReturnsFromService() throws Exception {
        // Given
        UUID applicationId = UUID.randomUUID();
        JsonPatchOperation op = new ReplaceOperation(new JsonPointer("/name"), TextNode.valueOf("New Name"));
        JsonPatch applicationPatch = new JsonPatch(Collections.singletonList(op));

        Application applicationReturned = new Application();
        UUID returnedId = UUID.randomUUID();
        applicationReturned.setId(returnedId);
        given(applicationService.patchApplication(eq(applicationId), argThat(new JsonPatchMatcher(applicationPatch)))).willReturn(applicationReturned);

        ApplicationDTO dtoExpected = applicationMapper.toApplicationDTO(applicationReturned);

        MockHttpServletRequestBuilder request = MockMvcRequestBuilders
                .patch("/api/v1/applications/" + applicationId)
                .contentType(Endpoint.JSON_PATCH)
                .content(objectMapper.writeValueAsString(applicationPatch));

        // When
        ResultActions response = mockMvc.perform(request);
        ApplicationDTO dtoActual = objectMapper.readValue(response.andReturn().getResponse().getContentAsString(), ApplicationDTO.class);

        // Then
        verify(applicationService).patchApplication(eq(applicationId), argThat(new JsonPatchMatcher(applicationPatch)));
        response.andExpect(status().isOk());
        assertEquals(dtoExpected, dtoActual);
    }

    @Test
    public void testDeleteApplicationCallsService() throws Exception {
        // Given
        UUID applicationId = UUID.randomUUID();

        MockHttpServletRequestBuilder request = MockMvcRequestBuilders
                .delete("/api/v1/applications/" + applicationId);

        // When
        ResultActions response = mockMvc.perform(request);

        // Then
        verify(applicationService).deleteApplication(applicationId);
        response.andExpect(status().isNoContent());
    }

}
