package com.gavinfenton.contrast.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.TextNode;
import com.gavinfenton.contrast.constant.Endpoint;
import com.gavinfenton.contrast.dto.OrganizationDTO;
import com.gavinfenton.contrast.entity.Organization;
import com.gavinfenton.contrast.mapper.OrganizationMapper;
import com.gavinfenton.contrast.service.OrganizationService;
import com.github.fge.jackson.jsonpointer.JsonPointer;
import com.github.fge.jsonpatch.JsonPatch;
import com.github.fge.jsonpatch.JsonPatchOperation;
import com.github.fge.jsonpatch.ReplaceOperation;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import utils.JsonPatchMatcher;
import utils.PageMatcher;
import utils.PageModule;

import java.util.Collections;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.argThat;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(OrganizationController.class)
public class OrganizationControllerTest {

    private final OrganizationMapper organizationMapper = OrganizationMapper.INSTANCE;

    private final ObjectMapper objectMapper = new ObjectMapper();

    @MockBean
    private OrganizationService organizationService;

    @Autowired
    private MockMvc mockMvc;

    @BeforeEach
    public void setup() {
        initMocks(this);

        objectMapper.registerModule(new PageModule());
    }

    @Test
    public void testCreateOrganizationCallsServiceWithAppAndReturnsFromServiceWithLocationHeader() throws Exception {
        // Given
        OrganizationDTO dtoCreating = new OrganizationDTO();
        dtoCreating.setName("Contrast");
        Organization organizationCreating = organizationMapper.toOrganization(dtoCreating);

        Organization organizationReturned = new Organization();
        UUID returnedId = UUID.randomUUID();
        organizationReturned.setId(returnedId);
        given(organizationService.createOrganization(organizationCreating)).willReturn(organizationReturned);

        OrganizationDTO dtoExpected = organizationMapper.toOrganizationDTO(organizationReturned);

        String url = "/api/v1/organizations";
        MockHttpServletRequestBuilder request = MockMvcRequestBuilders
                .post(url)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(dtoCreating));

        // When
        ResultActions response = mockMvc.perform(request);
        OrganizationDTO dtoActual = objectMapper.readValue(response.andReturn().getResponse().getContentAsString(), OrganizationDTO.class);

        // Then
        verify(organizationService).createOrganization(organizationCreating);
        response.andExpect(status().isCreated());
        response.andExpect(header().string("Location", url + "/" + returnedId));
        assertEquals(dtoExpected, dtoActual);
    }

    @Test
    public void testGetOrganizationsCallsServiceAndReturnsDTOPage() throws Exception {
        // Given
        int size = 25;
        Pageable pageRequest = PageRequest.of(0, size);

        Organization returnedOrganization = new Organization();
        returnedOrganization.setId(UUID.randomUUID());
        Page<Organization> returnedOrganizationPage = new PageImpl<>(Collections.singletonList(returnedOrganization), pageRequest, 1);
        given(organizationService.getOrganizations(pageRequest)).willReturn(returnedOrganizationPage);

        Page<OrganizationDTO> expectedDTOPage = returnedOrganizationPage.map(organizationMapper::toOrganizationDTO);

        MockHttpServletRequestBuilder request = MockMvcRequestBuilders
                .get("/api/v1/organizations?size=" + size);

        // When
        ResultActions response = mockMvc.perform(request);
        Page<OrganizationDTO> actualDTOPage = objectMapper.readValue(response.andReturn().getResponse().getContentAsString(), new TypeReference<>() {

        });

        // Then
        verify(organizationService).getOrganizations(pageRequest);
        response.andExpect(status().isOk());
        assertTrue(new PageMatcher(expectedDTOPage).matches(actualDTOPage));
    }

    @Test
    public void testGetOrganizationCallsServiceWithIdAndReturnsDTO() throws Exception {
        // Given
        UUID organizationId = UUID.randomUUID();

        Organization returnedOrganization = new Organization();
        returnedOrganization.setId(UUID.randomUUID());
        given(organizationService.getOrganization(organizationId)).willReturn(returnedOrganization);

        OrganizationDTO expectedDTO = organizationMapper.toOrganizationDTO(returnedOrganization);

        MockHttpServletRequestBuilder request = MockMvcRequestBuilders
                .get("/api/v1/organizations/" + organizationId);

        // When
        ResultActions response = mockMvc.perform(request);
        OrganizationDTO actualDTO = objectMapper.readValue(response.andReturn().getResponse().getContentAsString(), OrganizationDTO.class);

        // Then
        verify(organizationService).getOrganization(organizationId);
        response.andExpect(status().isOk());
        assertEquals(expectedDTO, actualDTO);
    }

    @Test
    public void testUpdateOrganizationCallsServiceWithAppAndReturnsFromService() throws Exception {
        // Given
        UUID organizationId = UUID.randomUUID();
        OrganizationDTO dtoUpdating = new OrganizationDTO();
        dtoUpdating.setName("Contrast");
        Organization organizationCreating = organizationMapper.toOrganization(dtoUpdating);

        Organization organizationReturned = new Organization();
        UUID returnedId = UUID.randomUUID();
        organizationReturned.setId(returnedId);
        given(organizationService.updateOrganization(organizationId, organizationCreating)).willReturn(organizationReturned);

        OrganizationDTO dtoExpected = organizationMapper.toOrganizationDTO(organizationReturned);

        MockHttpServletRequestBuilder request = MockMvcRequestBuilders
                .put("/api/v1/organizations/" + organizationId)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(dtoUpdating));

        // When
        ResultActions response = mockMvc.perform(request);
        OrganizationDTO dtoActual = objectMapper.readValue(response.andReturn().getResponse().getContentAsString(), OrganizationDTO.class);

        // Then
        verify(organizationService).updateOrganization(organizationId, organizationCreating);
        response.andExpect(status().isOk());
        assertEquals(dtoExpected, dtoActual);
    }

    @Test
    public void testPatchOrganizationCallsServiceWithAppAndReturnsFromService() throws Exception {
        // Given
        UUID organizationId = UUID.randomUUID();
        JsonPatchOperation op = new ReplaceOperation(new JsonPointer("/name"), TextNode.valueOf("New Name"));
        JsonPatch organizationPatch = new JsonPatch(Collections.singletonList(op));

        Organization organizationReturned = new Organization();
        UUID returnedId = UUID.randomUUID();
        organizationReturned.setId(returnedId);
        given(organizationService.patchOrganization(eq(organizationId), argThat(new JsonPatchMatcher(organizationPatch)))).willReturn(organizationReturned);

        OrganizationDTO dtoExpected = organizationMapper.toOrganizationDTO(organizationReturned);

        MockHttpServletRequestBuilder request = MockMvcRequestBuilders
                .patch("/api/v1/organizations/" + organizationId)
                .contentType(Endpoint.JSON_PATCH)
                .content(objectMapper.writeValueAsString(organizationPatch));

        // When
        ResultActions response = mockMvc.perform(request);
        OrganizationDTO dtoActual = objectMapper.readValue(response.andReturn().getResponse().getContentAsString(), OrganizationDTO.class);

        // Then
        verify(organizationService).patchOrganization(eq(organizationId), argThat(new JsonPatchMatcher(organizationPatch)));
        response.andExpect(status().isOk());
        assertEquals(dtoExpected, dtoActual);
    }

    @Test
    public void testDeleteOrganizationCallsService() throws Exception {
        // Given
        UUID organizationId = UUID.randomUUID();

        MockHttpServletRequestBuilder request = MockMvcRequestBuilders
                .delete("/api/v1/organizations/" + organizationId);

        // When
        ResultActions response = mockMvc.perform(request);

        // Then
        verify(organizationService).deleteOrganization(organizationId);
        response.andExpect(status().isNoContent());
    }

}
