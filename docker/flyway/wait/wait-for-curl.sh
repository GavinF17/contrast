#!/bin/sh
# Adapted from https://docs.docker.com/compose/startup-order/
# wait-for-curl.sh

set -e

host="$1"
shift
cmd="$@"

until curl $host; do
  >&2 echo "curl failed - sleeping"
  sleep 1
done

>&2 echo "curl failed- executing command"
exec $cmd