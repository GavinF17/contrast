DELETE
FROM organization;

DO
$$
    DECLARE
        org_id_1 UUID = uuid_generate_v4();
        org_id_2 UUID = uuid_generate_v4();
        org_id_3 UUID = uuid_generate_v4();
        org_id_4 UUID = uuid_generate_v4();
        org_id_5 UUID = uuid_generate_v4();
    BEGIN
        INSERT INTO organization (id, name)
        VALUES (org_id_1, 'Contrast'),
               (org_id_2, 'ABCompany'),
               (org_id_3, 'Microsoft'),
               (org_id_4, 'Deliveroo'),
               (org_id_5, 'Canonical');

        INSERT INTO application (name, organization_id, platform_id)
        VALUES ('Assess', org_id_1, 0),
               ('OSS', org_id_1, 0),
               ('Protect', org_id_1, 0),
               ('CE', org_id_1, 0),
               ('adoring sammet', org_id_2, 0),
               ('modest nobel', org_id_2, 0),
               ('serene hamilton', org_id_2, 0),
               ('serene hopper', org_id_2, 0),
               ('romantic panini', org_id_2, 2),
               ('elegant spence', org_id_2, 2),
               ('backstabbing payne', org_id_2, 0),
               ('hungry bassi', org_id_2, 0),
               ('nostalgic jang', org_id_2, 1),
               ('loving mcnulty', org_id_2, 1),
               ('suspicious bell', org_id_2, 1),
               ('amazing hopper', org_id_2, 1),
               ('amazing liskov', org_id_3, 0),
               ('condescending bardeen', org_id_3, 0),
               ('reverent mcclintock', org_id_3, 1),
               ('loving colden', org_id_3, 1),
               ('insane volhard', org_id_3, 2),
               ('romantic elion', org_id_3, 2),
               ('stupefied poincare', org_id_5, 1),
               ('insane mclean', org_id_5, 1),
               ('distracted sammet', org_id_5, 1),
               ('nostalgic bose', org_id_5, 1),
               ('backstabbing khorana', org_id_5, 1),
               ('thirsty ritchie', org_id_5, 1),
               ('tiny bardeen', org_id_5, 1),
               ('gloomy bose', org_id_5, 1);
    END
$$;