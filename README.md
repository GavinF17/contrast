# Contrast Java Engineer Project

## Running the Project
Just run `./build-and-run.sh`

This will:

- Clean up by removing ./target and previous JARs
- Run maven package to generate the JAR before moving it to ./docker
- Remove all docker-compose containers before rebuilding and bringing fresh ones up

## Considerations
Just some points to clarify any differences between the implementation and the requirements (if taken strictly).

- I have left the order/sort request parameter as sort and a comma separator rather than a space, as this is the Spring 
default, i.e. `&sort=name,asc` rather than `&order='name asc'`. If this were a real project I would have clarified 
whether this was a hard requirement.
- I have versioned the API, so the endpoints have been moved down a level by `/api/v1/...`, not starting like this has 
caused me pain with creating new versions before.
- `./build-and-run.sh` is currently destructive as mentioned above.
- `./docker/flyway/R__Seed_test_data.sql` is also destructive as it clears the Organization table, though the container 
removal already assures this.