#!/bin/sh

# Clean up previous
./mvnw clean
rm ./docker/app-service/contrast*.jar

# Package JAR
./mvnw package

# Move JAR
mv ./target/contrast*.jar ./docker/app-service/

# Clear up containers
docker-compose -f ./docker/docker-compose.yml rm -sf

## Start new containers
docker-compose -f ./docker/docker-compose.yml build
docker-compose -f ./docker/docker-compose.yml up